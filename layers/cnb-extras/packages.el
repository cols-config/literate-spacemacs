;;; packages.el --- Extra packages not loaded by Spacemacs layers
;;
;; Copyright (c) 2019 Colin Bell
;;
;; Author: Colin Bell <col@baibell.org>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defvar cnb-extras-packages
  '(
    all-the-icons-ivy
    ;; flycheck-posframe
    foreman-mode
    ;; ivy-posframe
    know-your-http-well
    magit-todos
    persistent-scratch
    writegood-mode))

;; From https://github.com/Yevgnen/ivy-rich/issues/37
(defun cnb-ivy-rich-switch-buffer-icon (candidate)
  (with-current-buffer
      (get-buffer candidate)
    (let ((icon (all-the-icons-icon-for-mode major-mode)))
      (if (symbolp icon)
          (all-the-icons-icon-for-mode 'fundamental-mode)
        icon))))

(defun cnb-extras/init-all-the-icons-ivy ()
  (use-package all-the-icons-ivy
    ;; :demand
    :after ivy-rich
    :config
    (all-the-icons-ivy-setup)
    (plist-put ivy-rich-display-transformers-list
               'ivy-switch-buffer
               '(:columns
                 ((cnb-ivy-rich-switch-buffer-icon :width 2 :delimiter "\t")
                  (ivy-rich-candidate (:width 30))
                  (ivy-rich-switch-buffer-size (:width 7))
                  (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
                  (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
                  (ivy-rich-switch-buffer-project (:width 15 :face success))
                  (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
                 :predicate
                 (lambda (cand) (get-buffer cand))))
    (ivy-rich-reload)))

;; (defun cnb-extras/init-flycheck-posframe ()
;;   (use-package flycheck-posframe
;;     :after flycheck
;;     :config
;;     (add-hook 'flycheck-mode-hook #'flycheck-posframe-mode)
;;     (flycheck-posframe-configure-pretty-defaults)))

;; (defun cnb-extras/init-ivy-posframe ()
;;   (use-package ivy-posframe
;;     :config
;;     (require 'ivy-posframe)
;;     (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display)))
;;     (ivy-posframe-mode 1)))

(defun cnb-extras/init-foreman-mode ()
  (use-package foreman-mode
    :config
    (evil-set-initial-state 'foreman-mode 'emacs)
    (spacemacs/set-leader-keys "of" 'foreman)))

(defun cnb-extras/init-know-your-http-well ()
  (use-package know-your-http-well))

(defun cnb-extras/init-magit-todos ()
  "Show TODOs in magit-status buffer."
  (use-package magit-todos
    :after magit-mode
    :config
    (magit-todos-mode)))

(defun cnb-extras/init-persistent-scratch ()
  "Save and restore the contents of the scratch buffer when Emacs \
starts/terminates. Don't allow scratch buffer to be killed."
  (use-package persistent-scratch
    :config
    (with-current-buffer "*scratch*"
      (emacs-lock-mode 'kill))

    (setq persistent-scratch-save-file
          (concat(file-name-as-directory spacemacs-cache-directory)
                 "persistent-scratch"))
    (persistent-scratch-setup-default)))

(defun cnb-extras/init-writegood-mode ()
  (use-package writegood-mode))
