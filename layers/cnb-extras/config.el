;;; config.el --- Emacs configuration
;;
;; Copyright (c) 2019 Colin Bell
;;
;; Author: Colin Bell <col@baibell.org>
;;
;; This file is not part of GNU Emacs.
;;

;; Me, Myself I.
(setq user-mail-address "col@baibell.org")
(setq user-full-name "Colin Bell")

;; Remove unnecessary clutter.
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(with-eval-after-load 'spaceline-segments
  (spaceline-toggle-minor-modes))

;; Keep mouse cursor away from text pointer
(mouse-avoidance-mode 'exile)

;; Flash frame instead of beeping
(setq visible-bell t)

;; Give a bit more space to line numbers. Makes long org-mode files look better.
(setq-default display-line-numbers-width 5)

;; Keep Emacs maintained configuration separate.
(setq custom-file (expand-file-name "custom.el" dotspacemacs-directory))
(if (file-exists-p custom-file)
  (load custom-file))

(setq-default indicate-buffer-boundaries 'left)

;; Always show tab characters.
(setq whitespace-style '(tab-mark))
(global-whitespace-mode)

(setq prettify-symbols-unprettify-at-point 'right-edge)
;; (global-prettify-symbols-mode)

;; Documents default to 80 characters wide.
(setq-default fill-column 80)

;; Single spacing, this isn't a type-writer.
(setq colon-double-space nil)
(setq sentence-end-double-space nil)

;; If saving a script file ensure that it is executable
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; When opening files follow all symbolic links.
(setq-default find-file-visit-truename t )

;; Visually indicate Evil operations.
(setq evil-goggles-pulse 'display-graphic-p)
(setq evil-goggles-async-duration nil)
(setq evil-goggles-blocking-duration nil)
(setq evil-goggles-duration 0.5)

;; Show fill column indicator in programming modes.
(if (fboundp 'display-fill-column-indicator-mode)
    (progn
      (setq-default display-fill-column-indicator-character ?\u2506)
      (set-face-attribute 'fill-column-indicator nil :foreground "#003f8e")
      (add-hook 'prog-mode-hook #'display-fill-column-indicator-mode))
  (add-hook 'prog-mode-hook #'fci-mode))

;; C-l first position to top.
(setq recenter-positions '(top middle bottom))

;; Don't save duplicates in command history, search history etc.
(setq history-delete-duplicates t)

;;; Better compilation configuration.

;; kill compilation process before starting another
(setq-default compilation-always-kill t)

;; Save all buffers on compile.
(setq-default compilation-ask-about-save nil)

;; auto-update IBuffer.
(defun cnb/ibuffer-hook-fn ()
  (ibuffer-auto-mode))
(add-hook 'ibuffer-mode-hooks #'cnb/ibuffer-hook-fn)

;; Don't use evil in ibuffer.
;; (evil-set-initial-state 'ibuffer-mode 'emacs)

;; Colourize colour names in programming modes.
(setq rainbow-html-colors 'auto)
(setq rainbow-x-colors 'auto)
(add-hook 'prog-mode-hook #'rainbow-mode)

;; Show Magit status in a large window.
(setq-default git-magit-status-fullscreen t)

;; Lets not wait for large diffs, I.E. large merges.
(setq magit-diff-expansion-threshold 5)

;; EditorConfig
;; (use-package editorconfig
;;   :defer t
;;   :init (add-to-list 'auto-mode-alist '("\\.editorconfig" . conf-unix-mode)))

;; Set timezones for helm-world-time.
(require 'time)
(setq display-time-world-list '(("Australia/Sydney" "Sydney")
                                ("Australia/Perth" "Perth")
                                ("America/Los_Angeles" "Los Angeles")
                                ("America/New_York" "New York")
                                ("Asia/Shanghai" "China")
                                ("Europe/Belfast" "Belfast")))


;; Dont allow winum to move to a different frame.
(setq winum-scope 'frame-local)

;; Autofix common mistakes
(define-abbrev-table
  'global-abbrev-table '(("teh" "the" nil 0)
                         ("tehy" "they" nil 0)
                         ("yuo" "you" nil 0)
                         ("yuor" "your" nil 0)))
(setq-default abbrev-mode t)

;; Configure dired listing.
(setq dired-listing-switches "-alhG --group-directories-first")

;; Allow editing of permissions in wdired.
(setq wdired-allow-to-change-permissions t)

;; Hide all dotfiles on C-x M-o
(require 'dired-x)
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

;; Work around for https://github.com/syl20bnr/spacemacs/issues/10410
(defun kill-minibuffer ()
  (interactive)
  (when (windowp (active-minibuffer-window))
    (evil-ex-search-exit)))
(add-hook 'mouse-leave-buffer-hook #'kill-minibuffer)

;; Allow paste into xterm etc.
(setq select-enable-primary t)

;; Large kill ring.
(setq kill-ring-max 500)

;; Undo bit by bit.
(setq evil-want-fine-undo "Yes")

;; Seems to be needed for evil to work with system clipboard
(fset 'evil-visual-update-x-selection 'ignore)

;; Automatically save data copied from the system clipboard into the kill ring
;; before killing Emacs data.
(setq save-interprogram-paste-before-kill t)

;; Let me right-click in terminal to show terminal menu.
(xterm-mouse-mode -1)


;;---------------------------------------------------
;;; Terminal
;;---------------------------------------------------

;; Allow Ctrl-A and CTRL-R to work in terminal.
(defun cnb/setup-term-mode ()
  (evil-local-set-key 'insert (kbd "C-a") 'cnb/send-C-a)
  (evil-local-set-key 'insert (kbd "C-r") 'cnb/send-C-r))

(defun cnb/send-C-a ()
  (interactive)
  (term-send-raw-string "\C-a"))

(defun cnb/send-C-r ()
  (interactive)
  (term-send-raw-string "\C-r"))

(add-hook 'term-mode-hook #'cnb/setup-term-mode)

;;---------------------------------------------------
;;; Searching
;;---------------------------------------------------

;; Use anzu in place of standard Emacs search/replace. It gives better
;; feedback.
(setq anzu-replace-to-string-separator " ⇒ ")
(global-set-key (kbd "M-%") #'anzu-query-replace)
(global-set-key (kbd "C-M-%") #'anzu-query-replace-regexp)

;; Recenter display after swiper
(defun cnb/swiper-recenter (&rest args)
  (recenter))
(advice-add 'swiper :after #'cnb/swiper-recenter)

;; Give ivy minibuffer more room.
(setq ivy-height 25)

;;---------------------------------------------------
;;; Recent files
;;---------------------------------------------------

;; Files to ignore in recent files.
(with-eval-after-load 'recentf
  (add-to-list 'recentf-exclude "~$")
  (add-to-list 'recentf-exclude "tmp")
  (add-to-list 'recentf-exclude "/ssh:")
  (add-to-list 'recentf-exclude "/sudo:")
  (add-to-list 'recentf-exclude "TAGS")
  (add-to-list 'recentf-exclude "/\\.git/.*\\'")
  (add-to-list 'recentf-exclude recentf-save-file)
  (add-to-list 'recentf-exclude "~/.emacs.d/elpa")
  (add-to-list 'recentf-exclude "~/.emacs.d/.cache"))

;;---------------------------------------------------
;;; Projectile
;;---------------------------------------------------

;; Cache project files in projectile for performance.
(setq projectile-enable-caching t)

;; Stuff I don't care to see in Projectile.
(with-eval-after-load 'projectile-mode
  (setq projectile-globally-ignored-directories
        (append '("node_modules" "_build")
                projectile-globally-ignored-directories)))

;; Show projectile lists by most recently active.
(setq projectile-sort-order (quote recently-active))

;; Spacemacs doesn't have a default key for showing a buffer list of just the
;; current projects buffers.
(spacemacs/set-leader-keys "oi" 'projectile-ibuffer)

;;---------------------------------------------------
;;; LSP
;;---------------------------------------------------

;; Setting this when loading the layer doesn't work.
(setq lsp-ui-doc-enable nil)

;;---------------------------------------------------
;;; Emacs Lisp
;;---------------------------------------------------

(remove-hook 'emacs-lisp-mode-hook 'auto-compile-mode)

;;---------------------------------------------------
;;; Ruby
;;---------------------------------------------------

;; Treat underscores as part of word.
(with-eval-after-load 'ruby-mode
  (modify-syntax-entry (string-to-char "_") "w" ruby-mode-syntax-table))

(add-hook 'ruby-mode-hook #'cnb/ruby-mode-hook t)

(defun cnb/ruby-mode-hook()
  "Custom `ruby-mode' behaviours."
  (setq prettify-symbols-alist
        '(
          ("==" . #x2a75)
          ("!=" . #x2260)
          ("->" . #x27f6)
          ("<-" . #x27f5)
          ("<=" . #x2a7d)
          (">=" . #x2a7e)
          ("=>" . 8658)
          ("::" . #x2E2C)
          ("lambda" . 955)))
  ;; (prettify-symbols-mode)
  )

;;---------------------------------------------------
;;; Elixir
;;---------------------------------------------------

;; Fixes problems with code reloading not working in
;; Elixir/Phoenix. See http://spacemacs.org/doc/FAQ.html#orgheadline18
(setq create-lockfiles nil)

;; Use web-mode for Phoenix Live View templates.
(add-to-list 'auto-mode-alist '("\\.leex\\'" . web-mode))

;; Use project configuration file for formatter.
(defun cnb/elixir-use-project-format ()
  (if (projectile-project-p)
      (setq elixir-format-arguments
            (list "--dot-formatter"
                  (concat
                   (locate-dominating-file buffer-file-name ".formatter.exs")
                   ".formatter.exs")))
    (setq elixir-format-arguments nil)))

(add-hook 'elixir-format-hook #'cnb/elixir-use-project-format)

(defun cnb/elixir-mode-hook()
  "Custom `elixir-mode' behaviours."
  ;; (setq prettify-symbols-alist
  ;;       '(
  ;;         ("lambda" . 955)
  ;;         ("==" . #x2a75)
  ;;         ("!=" . #x2260)
  ;;         ("->" . #x27f6)
  ;;         ("<-" . #x27f5)
  ;;         ("<=" . #x2a7d)
  ;;         (">=" . #x2a7e)
  ;;         ("=>" . 8658)
  ;;         ("::" . #x2E2C)
  ;;         ;; ("def" . ?ℱ)
  ;;         ("fn" . #x1d6cc)
  ;;         ;; ("<=" . 8656)
  ;;         ;; ("|>" . #x2937)
  ;;         ("|>" . #x2b9a)
  ;;         ))
  ;; (prettify-symbols-mode)

  ;; Match fill-column to match Elixir formatter.
  (set-fill-column 98))

(add-hook 'elixir-mode-hook #'cnb/elixir-mode-hook)

(with-eval-after-load 'elixir-mode
  ;; Treat underscores as part of word.
  (modify-syntax-entry (string-to-char "_") "w" elixir-mode-syntax-table)

;;   (spacemacs/declare-prefix-for-mode 'elixir-mode
;;     "mt" "tests" "testing related functionality")
;;   (spacemacs/set-leader-keys-for-major-mode 'elixir-mode
;;     "ta" 'exunit-verify-all
;;     "tb" 'exunit-verify
;;     "tk" 'exunit-rerun
;;     "tt" 'exunit-verify-single)
  )

;;---------------------------------------------------
;;; Conf mode
;;---------------------------------------------------

;; Show line numbers in Linux config files.
(add-hook 'conf-mode-hook #'linum-mode)

;;---------------------------------------------------
;;; Text mode
;;---------------------------------------------------
(add-hook 'text-mode-hook #'turn-on-auto-fill)

;;---------------------------------------------------
;;; CSS modes
;;---------------------------------------------------
(setq scss-compile-at-save nil)
(setq css-indent-offset 2)

;;---------------------------------------------------
;;; Web Mode
;;---------------------------------------------------

(defun cnb/web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

(add-hook 'web-mode-hook 'cnb/web-mode-hook t)

(setq emmet-indentation 2)

(with-eval-after-load 'web-mode
  (add-to-list 'web-mode-engines-alist '("elixir" . "\\.ex\\'"))
  (add-to-list 'web-mode-engines-alist '("elixir" . "\\.eex\\'"))
  (add-to-list 'web-mode-engines-alist '("elixir" . "\\.leex\\'")))

;;---------------------------------------------------
;;; Org Mode
;;---------------------------------------------------

(with-eval-after-load 'org
  (require 'ob-tangle)

  ;; Activate Easy templates. <s <TAB> etc.
  (require 'org-tempo)

  (setq org-directory "~/Dropbox/org/")
  (setq org-agenda-files
        (list (concat org-directory "personal.org")
              (concat org-directory "kwela.org")
              (concat org-directory "notes.org")))
  (setq org-todo-keywords
        (quote ((sequence "TODO(t)" "STARTED(n)" "|" "DONE(d!/!)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE"))))

  ;; Allow refiling to any agenda file.
  (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                   (org-agenda-files :maxlevel . 9))))

  (setq org-capture-templates
        '(("t" "todo" entry (file+headline (concat org-directory "personal.org") "Tasks")
           "* TODO [#A] %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")))

  (setq org-src-fontify-natively t
        org-src-window-setup 'current-window
        org-src-preserve-indentation t
        org-src-tab-acts-natively t)

  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("sh" . "src sh"))
  (add-to-list 'org-structure-template-alist '("md" . "src markdown")))

(provide 'config)
